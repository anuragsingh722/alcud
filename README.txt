=== Plugin Name ===
Contributors: anurag722
Donate link: anurag722@hotmail.com
Tags: contact, contact us, widget, leads, address, about, about us, page, wordpress, seo, local, leads, marketing, webmaster, webmasters, contact us form, contact form, web, websites, blog, post, social media, Facebook, twitter, linkedIn, share, sharing, phone, phone number, business, business name, contacts
Requires at least: 3.0.1
Tested up to: 4.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

AeroLeads Contact Us Details lets you add contact details in your sidebar as a widget. Simple yet effective.

== Description ==

Bored of old textual contact details, that you give on a seperate page that visitors often ignore and move on. 

Give your contact details a more prominent space in your wordpress blog, place it in your sidebar. No more struggling with icons/images for various components of your details. 

AeroLeads Contact Us Details lets you display contact details in your sidebar. Simply install the plugin, set your contact details in the form, use the widget to place it in your sidebar and Voila it displays your contact details to all your visitors. 

Designed on Google's new material-design guidelines, AL-CUD(that's what we call it), presents your visitors sleek and classy contact details widget.

AeroLeads Contact Us Details is a free addon to  <a href="http://inboundio.com/">Inboundio</a> and <a href="http://aeroleads.com/">AeroLeads</a> Software, which is the next generation Marketing solution and the only white label marketing software on the web using which you can launch your marketing software or agency in just 1 day. 

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `alcud.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. AeroLeads Contact Us Details  is now activated. Go to the AL-CUD menu and provide your contact details. 
4. To add widget, go to Dashboard -> Appearance -> Widgets.
5. Add the new widget, by dragging AeroLeads Contact Us Details widget, into your widget area.
6. The default title is "Contact Us", in case you want to customise the title of the widget, provide it in the form in Appearance -> Widgets -> AeroLeads Contact Us Details.

== Frequently Asked Questions ==

= I have filled in my Contact Details, but I cannot see it in anywhere on my blog. =

You need to include "AeroLeads Contact Us Details" Widget in your sidebar/widget-area. Follow the follwing steps in-order to achieve the desired results

1. Goto Appearance menu.
2. Drag the AeroLeads Contact Us Details widget in your widget area.
3. Provide a title for your widget. The default title is set to "Contact Us".

= What if I want to show a part of Contact Us details? =

Just leave the input box empty to remove it completely from your contact details in your widget.

== Screenshots ==

1. AeroLeads Contact Us Details Form.
2. AeroLeads Contact Us Details Sidebar Widget.

== Changelog ==

= 1.0.0 =
* First Release.