(function( $ ) {
	'use strict';

	$(function() {
		$(document).ready(function(){
			$(".input_row").on("click", function(){
				$(".al_wrap .input_row .input_col label").slideUp();
				$($(this).find(".input_col label")).slideDown().css("display","block");
			});

			$(".al_details_form").submit(function(event){
				event.preventDefault();
				var form_data = JSON.stringify($(".al_details_form").serializeJSON());
				console.log(form_data);
				var data = {
					action : 'add_details',
					form_data : form_data
				};

				$.post(ajaxurl, data)
				.success(function(){
					alert("Contact Saved");
				})
			});
		});
	});

	
})( jQuery );
