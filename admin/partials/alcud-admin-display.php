<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<section class="al_wrap">
	<header>
		<div class="container">
			<span class="page-header">AeroLeads Contact Us Details</span>
		</div>
	</header>
	<body>
		<div class="container">
			<form class="al_details_form" name="al_details_form" id="al_details_form">
				<div class="input_row">
					<i class="md md-work"></i>
					<div class="input_col">
						<label for="business_name">Business Name</label>
						<input type="text" class="form-control" name="business_name" id="business_name" placeholder="Enter Business Name" value="<?php echo $business_name ?>">
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-person"></i>
					<div class="input_col">
						<label for="contact_person">Contact Person</label>
						<input type="text" class="form-control" name="contact_person" id="contact_person" placeholder="Enter Contact Person Name" value="<?php echo $contact_person ?>">
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-phone"></i>
					<div class="input_col">
						<label for="contact_number">Contact Number</label>
						<input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="Enter Contact Number" value="<?php echo $contact_number ?>">
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-email"></i>
					<div class="input_col">
						<label for="email_address">Email Address</label>
						<input type="text" class="form-control" name="email_address" id="email_address" placeholder="Enter Email Address" value="<?php echo $email_address ?>">
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-location-on"></i>
					<div class="input_col">
						<label for="address">Address</label>
						<textarea type="text" class="form-control" name="address" id="address" placeholder="Enter Address" ><?php echo $address ?></textarea>
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-access-time"></i>
					<div class="input_col">
						<label for="open_hours">Open Hours</label>
						<input type="text" class="form-control" name="open_hours" id="open_hours" placeholder="Enter Open Hours" value="<?php echo $open_hours ?>">
					</div>
				</div>

				<!-- <br> -->
				<div class="input_row">
					<i class="md md-add"></i>
					<div class="input_col">
						<label for="note">Add Note</label>
						<textarea type="text" class="form-control" name="note" id="note" placeholder="Add Note" ><?php echo $note ?></textarea>
					</div>
				</div>

				<div class="input_row submit_row">
					<div class="input_col">
						<input type="submit" class="submit_btn">
						<input type="reset" class="submit_btn">
					</div>
				</div>

			</form>	
		</div>
	</body>
</section>